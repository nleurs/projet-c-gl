projet c++ / GL
L3 info G2
Leurs Nicolas
Labussière Hugo

![puissance4](http://www.pocketgamer.fr/images/database/1189-puissance-4_tb170.jpg)

Règles du jeu:

Le but du jeu est d'aligner une suite de 4 pions de même couleur sur une grille comptant 6 rangées et 7 colonnes. Chaque joueur dispose de 21 pions d'une couleur. Tour à tour les deux joueurs placent un pion dans la colonne de leur choix, le pion coulisse alors jusqu'à la position la plus basse possible dans la dite colonne à la suite de quoi c'est à l'adversaire de jouer. Le vainqueur est le joueur qui réalise le premier un alignement (horizontal, vertical ou diagonal) consécutif d'au moins quatre pions de sa couleur. Si, alors que toutes les cases de la grille de jeu sont remplies, aucun des deux joueurs n'a réalisé un tel alignement, la partie est déclarée nulle.


Comment jouer en local :

- Ouvrir un terminal dans le dossier projet-c-gl
- Exécuter la commande ./setup


Comment jouer en réseau local :

- Ouvrir un terminal dans le dossier projet-c-gl
- Exécuter la commande ./setupserv
- Exécuter la commande ./setupcli sur un autre terminal (par joueur)


Pour voir la documentation :

- Ouvrir le dossier projet-c-gl
- Ouvrir le dossier doc_doxygen
- Ouvrir le dossier html
- Ouvrir le fichier index.html