#include <uWS.h>
#include <iostream>
#include <string>
#include "Joueur.hpp"
#include "Grille.hpp"
#include "Jeu.hpp"

using PtrServerWs_t = uWS::WebSocket<uWS::SERVER> *;

PtrServerWs_t joueurR;
PtrServerWs_t joueurJ;
PtrServerWs_t joueurA;

Grille g;
Joueur p1=ROUGE;
Joueur p2=JAUNE;
Jeu j(p1,p2,g);

void serverOnConnection(PtrServerWs_t ws, uWS::HttpRequest)
{
  if(joueurR==0)
    {
      std::cout << "Connection Joueur ROUGE" << std::endl;
      joueurR=ws;
    }
  else
    {
      if(joueurJ==0)
	{
	  std::cout << "Connection Joueur JAUNE" << std::endl;
	  joueurJ=ws;
	  g.initGrille();
	  std::cout << std::endl;
	  std::cout << "DEBUT DE LA PARTIE" << std::endl;
	  g.afficherGrille();
	  std::string msg="C'est à vous\n"+g.grilleToString();
	  joueurJ->send(msg.c_str(),msg.size(),uWS::TEXT);
	}
      else
	{
	  std::string msg="partie pleine";
	  joueurA=ws;
	  joueurA->send(msg.c_str(),msg.size(),uWS::TEXT);
	  joueurA=0;
	}	   
    }	
}
  
void serverOnDisconnection(PtrServerWs_t ws, int, char*, size_t)
{  
  if(ws==joueurR)
    {
      if(joueurJ==0)
	{
	  joueurR=0;
	  std::cout << "Déconnexion client R" << std::endl;
	}
      else
	{
	  std::string msg="Victoire Jaune";
	  joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT);
	  joueurJ=0;
	  std::cout << "Déconnexion client R" << std::endl;
	  std::cout << "Victoire du Joueur Jaune" << std::endl;
	}
    }
  
  if(ws==joueurJ)
    {
      joueurJ=0;
      std::cout << "Déconnexion client J" << std::endl;
      std::cout << "Victoire du Joueur Rouge" << std::endl;
      std::string msg="Victoire rouge";
      joueurR->send(msg.c_str(), msg.size(), uWS::TEXT);
    }
   
  if(ws==joueurA)
    {
      joueurA=0;
      std::cout << "Déconnexion client spectateur" << std::endl;
    }
}


void serverOnMessage(PtrServerWs_t ws, char *message, size_t length, uWS::OpCode)
{
  //std::cout << "echo:" << std::string(message, length) << std::endl;
  //ws->send(message, length, uWS::TEXT);
  int x=atoi(std::string(message,length).c_str());
  x=x-1;
  
  if(x>6 || x<0)//Si x est en dehors de la grille
    {
      std::string inv="Colonne invalide, il n'y a que 7 colonnes :\n"+g.grilleToString();
      ws->send(inv.c_str(), inv.size(), uWS::TEXT); 
    }
  else
    {
      if(ws==joueurJ)
	{
	  std::cout << "Echo : " << x << std::endl;
	  int y=0;
	  if(g.colonnePleine(x)==true)//Si la colonne est pleine
	    {
	      std::cout << "Colonne pleine" << std::endl;
	      std::string msg="C'est à vous\n"+g.grilleToString();
	      joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT); 
	    }
	  else
	    {
	      while(y<5)
		{
		  if(g.caseVide(x,y)==false)
		    {
		      break;
		    }
		  y=y+1;
		}
	      g.setGrille(y,x,'J');
	      g.afficherGrille();
	      if(g.grillePleine()==true)
		{
		  std::cout<<"Egalité"<<std::endl;
		  std::string msg="Egalité\n"+g.grilleToString()+"\n"+"nouvellepartie\n";
		  g.initGrille();
		  joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT);
		}
	      else
		{
		  if(j.victoire(x,y,g)==2)
		    {
		      std::cout << std::endl;
		      std::cout<<"Victoire Joueur Jaune"<<std::endl;
		      std::string msg="victoire jaune";
		      g.initGrille();
		      joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT);
		      msg="Victoire jaune";
		      joueurR->send(msg.c_str(), msg.size(), uWS::TEXT);
		    }
		  else
		    {
		      std::string msg="C'est à vous\n"+g.grilleToString();
		      joueurR->send(msg.c_str(), msg.size(), uWS::TEXT);
		    }
		}
	    }
	}
    
  
      if(ws==joueurR)
	{
	  std::cout << "Echo : " << std::string(message, length) << std::endl;
	  int y=0;
  
	  if(g.colonnePleine(x)==true)//Si la colonne est pleine
	    {
	      std::cout << "Colonne pleine" << std::endl;
	      std::string msg="C'est à vous\n"+g.grilleToString();
	      joueurR->send(msg.c_str(), msg.size(), uWS::TEXT); 
	    }
	  else
	    {
	      while(y<5)
		{
		  if(g.caseVide(x,y)==false)
		    {
		      break;
		    }
		  y=y+1;
		}
	      g.setGrille(y,x,'R');
	      g.afficherGrille();
	      if(g.grillePleine()==true)
		{
		  std::cout << "Egalité" << std::endl;
		  std::string msg="Egalité\n"+g.grilleToString()+"\n"+"Nouvelle Partie\n";
		  g.initGrille();
		  joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT);
		}	   
	      else
		{
		  if(j.victoire(x,y,g)==1)
		    {
		      std::cout << "Victoire joueur Rouge" << std::endl;
		      std::string msg="victoire rouge";
		      g.initGrille();
		      joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT);
		      msg="Victoire rouge";
		      joueurR->send(msg.c_str(), msg.size(), uWS::TEXT);
		    }
		  else
		    {
		      std::string msg="C'est à vous\n"+g.grilleToString();
		      joueurJ->send(msg.c_str(), msg.size(), uWS::TEXT);
		    }
		}
	    }	      
	}
    }
}

int main()
{
  std::cout << std::endl;
  uWS::Hub hub;
  hub.onConnection(serverOnConnection);
  hub.onMessage(serverOnMessage);
  hub.onDisconnection(serverOnDisconnection);
  hub.onMessage(serverOnMessage);
  hub.listen(3000);
  std::cout << "Serveur ok" << std::endl;
  hub.run();
  return 0;
}
