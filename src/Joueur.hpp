
#ifndef JOUEUR_HPP_
#define JOUEUR_HPP_
#include <iostream>

//Joueur ne possédant pas de classe, il n'est pas possible de le présenter dans la documentation développeur de Doxygen

///Enumeration précisant les valeurs que peuvent prendre les Joueur.
enum Joueur : char {LIBRE, ROUGE, JAUNE, PERSONNE};

std::ostream & operator<<(std::ostream & os, Joueur joueur);
std::istream & operator>>(std::istream & is, Joueur & joueur);

///Fonction permettant de retourner la valeur du Joueur entré en paramètre.
std::string formaterJoueur(Joueur joueur);

#endif
