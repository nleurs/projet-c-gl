#include <uWS.h>
#include <iostream>
using PtrClientWs_t = uWS::WebSocket<uWS::CLIENT> *;

int nbVictoireJ=0;
int nbVictoireR=0;

void saisirEtEnvoyer(PtrClientWs_t ws)
{
  //Saisie d'un message au clavier
  std::cout << "Entrez une colonne (quit pour quitter) : ";
  std::string msg;
  getline(std::cin, msg);
  
  //Quitte si le message est quit
  if (msg == "quit")
    {
      exit(0);
    }
    
  //Sinon envoie le message au serveur
  ws->send(msg.c_str(), msg.size(), uWS::TEXT);
}

void clientOnConnection(PtrClientWs_t ws, uWS::HttpRequest)//Fonction appelée une fois, lorsque le serveur accepte la connexion
{
  std::cout << std::endl;
  std::cout << "Connexion ok" << std::endl;
}

void clientOnMessage(PtrClientWs_t ws, char *message, size_t length, uWS::OpCode)//Fonction appelée à chaque fois qu'on reçoit un message du serveur
{
  std::cout << std::string(message, length) << std::endl;
  std::string plein="partie pleine";
  std::string victoireJ="victoire jaune";
  std::string victoireR="victoire rouge";
  std::string VictoireJ="Victoire jaune";
  std::string VictoireR="Victoire rouge";
  if(std::string(message,length)==plein)
    {
      ws->close();
    }
  else
    {
      if(std::string(message,length)==victoireJ)
	{
	  nbVictoireJ++;
	  std::cout << " Nombre de victoire Jaune : " << nbVictoireJ << std::endl;
	  std::cout << " Nombre de victoire Rouge : " << nbVictoireR << std::endl;
	  std::cout << std::endl;
	  std::cout << " Nouvelle Partie" << std::endl;
	  std::cout << std::endl;
	  saisirEtEnvoyer(ws);
	}
      else
	{
	  if(std::string(message,length)==victoireR)
	    {
	      nbVictoireR++;
	      std::cout << " Nombre de victoire Jaune : " << nbVictoireJ << std::endl;
	      std::cout << " Nombre de victoire Rouge : " << nbVictoireR << std::endl;
	      std::cout << std::endl;
	      std::cout << " Nouvelle Partie" << std::endl;
	      std::cout << std::endl;
	      saisirEtEnvoyer(ws);
	    }
	  else
	    {
	      if(std::string(message,length)==VictoireR)
		{
		  nbVictoireR++;
		  std::cout << " Nombre de victoire Jaune : " << nbVictoireJ << std::endl;
		  std::cout << " Nombre de victoire Rouge : " << nbVictoireR << std::endl;
		  std::cout << std::endl;
		  std::cout << " Nouvelle Partie (en attente du vote du joueur Jaune)"+nbVictoireR << std::endl;
		  std::cout << std::endl;
		}
	      else
		{
		  if(std::string(message,length)==VictoireJ)
		    {
		      nbVictoireJ++;
		      std::cout << " Nombre de victoire Jaune : " << nbVictoireJ << std::endl;
		      std::cout << " Nombre de victoire Rouge : " << nbVictoireR << std::endl;
		      std::cout << std::endl;
		      std::cout << " Nouvelle Partie (en attente du vote du joueur Jaune)"+nbVictoireR << std::endl;
		      std::cout << std::endl;
		    }
		  else
		    {
		      saisirEtEnvoyer(ws);
		    }
		}
	    }
	}
    }
}

int main()
{
  uWS::Hub hub;
  hub.onConnection(clientOnConnection);
  hub.onMessage(clientOnMessage);
  //hub.connect("ws://echo.websocket.org:80", nullptr);
  hub.connect("ws://localhost:3000", nullptr);
  hub.run();
  return 0;
}

