#ifndef Jeu_HPP
#define Jeu_HPP

#include "Grille.hpp"
#include "Joueur.hpp"

//! Permet de construire et de lancer le Jeu.

/// Elle est composée d'une Grille et de deux Joueur.

class Jeu
{
  
public:

  ///Joueur 1 : ROUGE
  Joueur _p1;

  ///Joueur 2 : JAUNE
  Joueur _p2;

  ///Grille de jeu
  Grille _g;

  ///Constructeur du Jeu
  Jeu(Joueur p1,Joueur p2,Grille g);

  ///Fonction mère qui permet de lancer une partie
  void Jouer();

  ///Fonction qui va retourner si il y a victoire d'un joueur quand un jeton est placé dans la grille
  int victoire(int x,int y,Grille g);

};

#endif
