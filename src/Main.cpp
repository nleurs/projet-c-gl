#include <iostream>
#include <string>
#include "Joueur.hpp"
#include "Grille.hpp"
#include "Jeu.hpp"

int main()
{
  Joueur p1=ROUGE;
  Joueur p2=JAUNE;
  std::cout << std::endl;
  std::cout << "Joueur 1 : " << formaterJoueur(p1) << std::endl;
  std::cout << "Joueur 2 : " << formaterJoueur(p2) << std::endl;
  
  Grille g;//Création de la Grille g
  g.initGrille();//Initialisation de la Grille g
  Jeu j(p1,p2,g);//Création d'un Jeu j
  j.Jouer();//Lancement du Jeu j
}

/*! \mainpage Bienvenue sur la documentation développeur du projet "Puissance 4" !
 *
 * <BR><BR>
 * 
 * \section pres_sec Présentation
 * Le but de ce projet est de développer un jeu de Puissance 4 en réseau à 2 
 * Joueurs avec interface graphique, voici la présentation du Jalon 1 sur 3 
 * de ce projet (Moteur du jeu)
 *
 * <BR><BR>
 * 
 * \section exec_sec Execution
 * Dans le dossier projet-c-gl, ouvrez un terminal, puis executez la 
 * commande ./setup, le jeu compile puis se lance, et place au jeu !
 *
 * <BR><BR>
 * 
 */
