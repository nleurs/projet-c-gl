#include <iostream>
#include "Grille.hpp"

#define col 6
#define lig 7

Grille::Grille()//Création d'une grille 6*7
{
  grille.resize(col*lig);
}

int Grille::getIndex(int x,int y)
{
  return (x+y*lig);
}

void Grille::setGrille(int x,int y,char c)
{
  if(x>=0 and y>=0 and x<col and y<lig)
    {	
      grille[x+y*col]=c;
    }
  else
    {
      std::cout << "ERREUR: Coordonnées non valides" << std::endl;
    }
}

char Grille::getGrille(int x,int y)
{
  if(x>=0 and y>=0 and x<col and y<lig)
    {	
      return grille[x+y*col];
    }
  else
    {
      return 'n';
    }
}

void Grille::initGrille()
{
  for(int i=0;i<6;i++)
    {
      for(int j=0;j<7;j++)
	{
	  setGrille(i,j,'.');
	}
    }
}

void Grille::afficherGrille()
{
  std::cout<<std::endl;
  std::cout<<"1 2 3 4 5 6 7";
  for(int i=0;i<6;i++)//Pour toutes les lignes
    {
      std::cout << std::endl;
      for(int j=0;j<7;j++)//Pour toutes les colonnes
	{
	  std::cout << getGrille(i,j) << " ";//Afficher la cellule correspondante
	}
    }
  std::cout << std::endl;
}

bool Grille::colonnePleine(int x)
{
  int y=0;
  if(getGrille(y,x)=='R' || getGrille(y,x)=='J')
    {
      return true;
    }
  else
    {
      return false;
    }
}

bool Grille::caseVide(int x,int y)
{
  if(getGrille(y+1,x)=='.')
    {
      return true;
    }
  else
    {
      return false;
    }
}

bool Grille::grillePleine()
{
  int i=0;
  while(colonnePleine(i)==true)
    {
      if(i==6)
	{
	  return true;
	}
      i++;
    }
  return false;  
}

int Grille::lignePlus(int x,int y,int cpt)
{
  if(getGrille(y,x+1)==getGrille(y,x))
    {
      return lignePlus(x+1,y,cpt+1);
    }
  else
    {
      return cpt;
    }
}

int Grille::ligneMoins(int x,int y,int cpt)
{
  if(getGrille(y,x-1)==getGrille(y,x))
    {
      return ligneMoins(x-1,y,cpt+1);
    }
  else
    {
      return cpt;
    }
}

int Grille::colonnePlus(int x,int y,int cpt)
{
  if(getGrille(y+1,x)==getGrille(y,x))
    {
      return colonnePlus(x,y+1,cpt+1);
    }
  else
    {
      return cpt;
    }
}

int Grille::colonneMoins(int x,int y,int cpt)
{
  if(getGrille(y-1,x)==getGrille(y,x))
    {
      return colonneMoins(x,y-1,cpt+1);
    }
  else
    {
      return cpt;
    }
}
  
int Grille::diagonaleDroitePlus(int x,int y,int cpt)
{
  if(getGrille(y-1,x+1)==getGrille(y,x))
    {
      return diagonaleDroitePlus(x+1,y-1,cpt+1);
    }
  else
    {
      return cpt;
    }
}

int Grille::diagonaleDroiteMoins(int x,int y,int cpt)
{
  if(getGrille(y+1,x-1)==getGrille(y,x))
    {
      return diagonaleDroiteMoins(x-1,y+1,cpt+1);
    }
  else
    {
      return cpt;
    }
}

int Grille::diagonaleGauchePlus(int x,int y,int cpt)
{
  if(getGrille(y-1,x-1)==getGrille(y,x))
    {
      return diagonaleGauchePlus(x-1,y-1,cpt+1);
    }
  else
    {
      return cpt;
    }
}

int Grille::diagonaleGaucheMoins(int x,int y,int cpt)
{
  if(getGrille(y+1,x+1)==getGrille(y,x))
    {
      return diagonaleGaucheMoins(x+1,y+1,cpt+1);
    }
  else
    {
      return cpt;
    }
}

std::string Grille::grilleToString()
{
  std::string s;
  s=s+"1234567\n";
  for(int i=0;i<6;i++)
    {
      for(int j=0;j<7;j++)
	{
	  s=s+getGrille(i,j);
	}
      s=s+"\n";
    }
  return s;
}
	

