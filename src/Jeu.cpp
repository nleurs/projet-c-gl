#include <iostream>
#include "Jeu.hpp"
#include <stdio.h>
#include <string>
#include <boost/lexical_cast.hpp>

Jeu::Jeu(Joueur p1,Joueur p2,Grille g)//Un Jeu contient 2 Joueurs (p1 et p2) et une Grille (g)
{
  _p1=p1;
  _p2=p2;
  _g=g;
}

void Jeu::Jouer()
{
 debut:

  _g.initGrille();//Initialisation de la Grille (remplissage par des points)
  _g.afficherGrille();//Affichage de la Grille "vide" (remplie de points)
  int x=0;//Colonnes de la Grille
  int y=0;//Lignes de la Grille

 tour_j1://Début du tour du joueur 1
  
  y=0;
  
  std::cout << std::endl;
  std::cout << "Tour du Joueur 1" << std::endl;
  std::cout << "Où voulez vous placer votre pion ? (Numéro de colonne) : ";
  std::cin >> x;//Saisie de la colonne où le pion sera placé pour le Joueur 1 "Rouge"
  x=x-1;//Réduction de 1 pour faciliter l'utilisation pour une personne non initiée aux pratiques de la programmation
   
    while(!(std::cin))//Si x n'est pas un char
      {
    std::cout << "Merci de bien vouloir choisir un nombre entre 1 et 7 : ";
    std::cin.clear();
    std::cin.ignore(999,'\n');
    std::cin >> x;//Nouvelle saisie du numéro de la colonne où placer le pion
    }
  
   if(x>6 || x<0)//Si x est en dehors de la grille
    {
      std::cout << "Colonne invalide, il n'y a que 7 colonnes : ";
      std::cin >> x;//Nouvelle saisie du numéro de la colonne où placer le pion
      goto tour_j1;//Retourne au début du tour du Joueur 1 "Rouge"
    }
   
  if(_g.colonnePleine(x)==true)//Si la colonne est pleine
    {
      std::cout << "Colonne pleine" << std::endl;
      goto tour_j1;
    }
  else
    {
      while(y<5)//Recherche si les cases en dessous sont vides pour faire descendre le pion le plus possible dans la colonne
	{
	  if(_g.caseVide(x,y)==false)
	    {
	      break;  
	    }
	  y=y+1;
	}
      _g.setGrille(y,x,'R');
    }

  std::cout << std::endl;
  std::cout << "---------------------------------------------------------------------------" << std::endl;
  _g.afficherGrille();
  
  if(_g.grillePleine()==true)
    {
      goto exec;//aller à exec
    }
  
  if(victoire(x,y,_g)==1)
    {
      goto VR;//aller à VR
    }
  
  if(victoire(x,y,_g)==2)
    {
      goto VJ;//aller à VJ
    }

 tour_j2:
  
  y=0;
  
  std::cout << std::endl;
  std::cout << "Tour du Joueur 2" << std::endl;
  std::cout << "Où voulez vous placer votre pion ? (Numéro de colonne) : ";
  std::cin >> x;//Saisie de la colonne où le pion sera placé pour le Joueur 2
  x=x-1;//Réduction de x pour faciliter l'utilisatio pour une personne non initiée aux pratiques de la programmation

   while(!(std::cin))//Si x n'est pas un char
     {
    std::cout << "Merci de bien vouloir choisir un nombre entre 1 et 7 : ";
    std::cin.clear();
    std::cin.ignore(999,'\n');
    std::cin >> x;
   }
   
   if(x>6 || x<0)//Si x est en dehors de la grille
    {
      std::cout << "Colonne invalide, il n'y a que 7 colonnes : ";
      std::cin >> x;//Nouvelle saisie du numéro de la colonne où placer le pion
      goto tour_j2;//Retourne au début du tour du Joueur 2
    }
   
   if(_g.colonnePleine(x)==true)//Si la colonne est pleine
    {
      std::cout << "Colonne pleine" << std::endl;
      goto tour_j2;
    }
   
  else
    {
      while(y<5)//Recherche si les cases en dessous sont vides pour faire descendre le pion le plus possible dans la colonne
	{
	  if(_g.caseVide(x,y)==false)
	    {
	      break;  
	    }
	  y=y+1;
	}
      _g.setGrille(y,x,'J');
    }
  
  std::cout << std::endl;
  std::cout << "---------------------------------------------------------------------------" << std::endl;
  _g.afficherGrille();
   
  if(_g.grillePleine()==true)
    {
      goto exec;
    }
  
  if(victoire(x,y,_g)==1)
    {
      goto VR;
    }
 
  if(victoire(x,y,_g)==2)
    {
      goto VJ;
    }
  
  else
    {
      goto tour_j1;
    }

 VR://Victoire du Joueur Rouge

  std::cout << std::endl;
  std::cout << "Victoire du Joueur 1 (Rouge)"<<std::endl;
  goto cinnew;//Aller à cinnew

 VJ://Victoire du Joueur Jaune

  std::cout << std::endl;
  std::cout << "Victoire du Joueur 2 (Jaune)"<<std::endl;
  goto cinnew;//Aller à cinnew

 exec://Egalité (ex-aequo)
  
  std::cout << "Egalité, la grille est pleine, plus aucun coup ne peut être joué" << std::endl;

 cinnew://Demande si on veut refaire une autre partie
  
  std::cout << "Voulez vous refaire une partie ? (Entrez Oui ou Non) : ";
  std::string new_game;
  
  std::cin >> new_game;
  if (new_game !="NON" && new_game!="OUI" &&  new_game !="Non" && new_game!="Oui" &&  new_game !="non" && new_game!="oui")
    {
      std::cout << "Entrée invalide" << std::endl;
      goto cinnew;
    }
  if(new_game=="OUI" || new_game=="Oui" || new_game=="oui")
    {
      std::cout << std::endl;
      std::cout << "---------------------------------------------------------------------------" << std::endl;
      std::cout << std::endl;
      std::cout << "NOUVELLE PARTIE"<< std::endl;
      goto debut;
    }
  if(new_game=="NON" || new_game=="Non" || new_game=="non")
    {
      std::cout << "Au revoir" << std::endl;
    }
}

int Jeu::victoire(int x,int y,Grille g)
{
  int cpt=0;
  //Séparation des 3 conditions pour plus de lisibilité
  if(g.getGrille(y,x)=='R')//Tests de victoire de R
    {
      if(g.lignePlus(x,y,cpt)+g.ligneMoins(x,y,cpt)+1==4)
	{
	  return 1;
	}
      if(g.colonnePlus(x,y,cpt)+g.colonneMoins(x,y,cpt)+1==4)
	{
	  return 1;
	}
      if(g.diagonaleDroitePlus(x,y,cpt)+g.diagonaleDroiteMoins(x,y,cpt)+1==4)
	{
	  return 1;
	}
      if(g.diagonaleGauchePlus(x,y,cpt)+g.diagonaleGaucheMoins(x,y,cpt)+1==4)
	 {
	  return 1;
	 }
      return 0;
    }
  
  if(g.getGrille(y,x)=='J')//Tests de victoire de J
    {
      if(g.lignePlus(x,y,cpt)+g.ligneMoins(x,y,cpt)+1==4)
	{
	  return 2;
	} 
      if(g.colonnePlus(x,y,cpt)+g.colonneMoins(x,y,cpt)+1==4)
	{
	  return 2;
	}
       if(g.diagonaleDroitePlus(x,y,cpt)+g.diagonaleDroiteMoins(x,y,cpt)+1==4)
	{
	  return 2;
	}
       if(g.diagonaleGauchePlus(x,y,cpt)+g.diagonaleGaucheMoins(x,y,cpt)+1==4)
	 {
	  return 2;
	 }
      return 0;
    }
  
  return 666;//Case incorrecte
}
