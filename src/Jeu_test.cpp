#include <CppUTest/CommandLineTestRunner.h>
#include "Jeu.hpp"
#include <sstream>

TEST_GROUP(GroupJeu) {};

TEST(GroupJeu, Jeu_Victoire)//Test victoire(int x,int y,Grille g)
{
  Joueur p1=ROUGE;
  Joueur p2=JAUNE;
  Grille g;
  Jeu j(p1,p2,g);
  
  g.initGrille();
  g.setGrille(2,1,'R');
  g.setGrille(2,2,'R');
  g.setGrille(2,3,'R');
  g.setGrille(2,4,'R');
  
  int result1=j.victoire(2,2,g);
  CHECK_EQUAL(1,result1);//Test ligne
    
  g.initGrille();
  g.setGrille(1,3,'J');
  g.setGrille(2,3,'J');
  g.setGrille(3,3,'J');
  g.setGrille(4,3,'J');
  int result2=j.victoire(3,2,g);
  CHECK_EQUAL(2,result2);//Test colonne
    
  g.initGrille();
  g.setGrille(1,5,'R');
  g.setGrille(2,4,'R');
  g.setGrille(3,3,'R');
  g.setGrille(4,2,'R');
  int result3=j.victoire(3,3,g);
  CHECK_EQUAL(1,result3);//Test diagonale droite

  g.initGrille();
  g.setGrille(5,5,'J');
  g.setGrille(4,4,'J');
  g.setGrille(3,3,'J');
  g.setGrille(2,2,'J');
  bool result4=j.victoire(3,3,g);
  CHECK_EQUAL(1,result4);//Test diagonale gauche

  g.initGrille();
  g.setGrille(1,5,'R');
  g.setGrille(2,4,'R');
  g.setGrille(3,3,'R');
  g.setGrille(4,2,'J');
  int result5=j.victoire(3,3,g);
  CHECK_EQUAL(0,result5);//Test diagonale droite

  g.initGrille();
  g.setGrille(5,5,'R');
  g.setGrille(4,4,'J');
  g.setGrille(3,3,'R');
  g.setGrille(2,2,'R');
  int result6=j.victoire(3,3,g);
  CHECK_EQUAL(0,result6);//Test diagonale gauche
}
