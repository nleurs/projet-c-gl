#ifndef GRILLE_HPP
#define GRILLE_HPP
#include <string>
#include <vector>

//! Définit et modifie la Grille lors d'une interaction avec une d'elles

class Grille
{
private:
  std::vector<char>grille;
public:

  ///Constructeur de la grille
  Grille();

  ///Retourne l'index de la cellule par rapport à ses coordonnées ( sa position dans la grille )
  int getIndex(int x,int y);

  ///Permet de modifier la valeur de la case donnée dans la grille
  void setGrille(int x,int y,char c);

  ///Permet de retourner la valeur de la case donnée dans la grille
  char getGrille(int x,int y);

  ///Permet d'initialiser la grille avec les indices des colonnes, et de remplir celle-ci de "X" ( valeur par défaut )
  void initGrille();

  ///Permet d'afficher la grille du jeu
  void afficherGrille();

  ///Booléen qui retourne si la colonne est vide
  bool caseVide(int x,int y);

  ///Booléen qui retourne si la colonne est pleine pour que le joueur place un jeton là où il peut
  bool colonnePleine(int x);

  ///Booléen qui retourne si la colonne est pleine pour ne pas que le joueur ne place un jeton là où il ne peut pas
  bool grillePleine();

   ///Fonction qui va retourner une valeur permettant de savoir si 4 jetons sont alignés sur la ligne aux indices supérieurs de la Grille
  int lignePlus(int x,int y,int cpt);

  ///Fonction qui va retourner une valeur permettant de savoir si 4 jetons sont alignés sur la ligne aux indices inférieurs de la Grille
  int ligneMoins(int x,int y,int cpt);

  ///Fonction qui va retourner une valeur permettant de savoir si 4 jetons sont alignés sur la colonne aux indices supérieurs de la Grille
  int colonnePlus(int x,int y,int cpt);

  ///Fonction qui va retourner une valeur permettant de savoir si 4 jetons sont alignés sur la colonne aux indices inférieurs de la Grille
  int colonneMoins(int x,int y,int cpt);

  ///Fonction qui va retourner une valeur permettant de savoir si 4 jetons sont alignés sur la diagonale aux indices supérieurs de x et aux indices inférieurs de y dans la Grille
  int diagonaleDroitePlus(int x,int y,int cpt);

  ///Fonction qui va retourner ue valeur permettant de savoir si 4 jetons sont alignés sur la diagonale aux indices supérieurs de y et aux indices inférieurs de x dans la Grille
  int diagonaleDroiteMoins(int x,int y,int cpt);

  ///Fonction qui va retourner une valeur permettant de savoir si 4 jetons sont alignés sur la diagonale aux indices inférieurs de x et aux indices inférieurs de y dans la Grille
  int diagonaleGauchePlus(int x,int y,int cpt);
  
   ///Fonction qui va retourner ue valeur permettant de savoir si 4 jetons sont alignés sur la diagonale aux indices supérieurs de y et aux indices supérieurs de x dans la Grille
  int diagonaleGaucheMoins(int x,int y,int cpt);

  ///Fonction qui retourne la Grille actuelle sous forme de string 
  std::string grilleToString();
  
};

#endif
