#include <CppUTest/CommandLineTestRunner.h>

#include "Grille.hpp"

#include <sstream>

TEST_GROUP(GroupGrille) { };

TEST(GroupGrille, Grille_init) 
{
    Grille g;
    g.initGrille();//initialisation
    for(int i=0;i<6;i++)
    {
      for(int j=0;j<7;j++)
	{
	  CHECK(g.getGrille(i,j)=='.');
	}
    }
}

TEST(GroupGrille, Grille_setGrille)
{
  Grille g;
  g.initGrille();

  g.setGrille(0,0,'R');
  g.setGrille(1,1,'J');
  g.setGrille(2,2,'R');
  g.setGrille(3,3,'J');
  
  CHECK(g.getGrille(0,0)=='R');
  CHECK(g.getGrille(1,1)=='J');
  CHECK(g.getGrille(2,2)=='R');
  CHECK(g.getGrille(3,3)=='J');
}

TEST(GroupGrille, Grille_colonneVide)
{
  Grille g;
  g.initGrille();
  g.setGrille(0,2,'R');

  bool pleine1=g.caseVide(0,3);
  CHECK_EQUAL(true,pleine1);
  bool pleine2=g.caseVide(4,5);
  CHECK_EQUAL(false,pleine2);

 
}


TEST(GroupGrille, Grille_grillePleine)
{
  Grille g;
  g.initGrille();
  CHECK(g.grillePleine()==false);
  for(int i=0;i<6;i++)
    {
      for(int j=0;j<7;j++)
	{
	  g.setGrille(i,j,'R');
	}
    }
  CHECK(g.grillePleine()==true);
}

TEST(GroupGrille, Grille_colonnePleine)
{
  Grille g;
  g.initGrille();
  g.setGrille(0,3,'R');
  g.setGrille(1,3,'R');
  g.setGrille(2,3,'R');
  g.setGrille(3,3,'R');
  g.setGrille(4,3,'R');
  g.setGrille(5,3,'R');
  
  bool pleine1=g.colonnePleine(3);
  CHECK_EQUAL(true,pleine1);
  bool pleine2=g.colonnePleine(2);
  CHECK_EQUAL(false,pleine2);
}


